# Initialize

```sh
git clone git@gitlab.com:apitre/chess.git
cd  chess
npm install
```

# Dev command

```sh
npm run dev
```

# Build command (production)

```sh
npm run build -- --dist /Absolute/Path/To/Your/Folder
```

# Analyze build command

```sh
npm run analyze
```