import { expect } from 'chai';
import 'mocha';
import Moves from "Chess/Moves";
import Player from "Chess/Players/Player";
import Board from "Chess/Board";
import Piece from "Chess/Pieces/Piece";
import { ColorName } from 'Chess/Util';
import Square from 'Chess/Square';


describe("Test Moves", () => {

    var white: Player;
    var black: Player;
    var board: Board;
    var moves: Moves;

    beforeEach(() => {
        white = <Player>{}
        black = <Player>{}
        board = <Board>{}
        moves = new Moves(white, black, board);
    });

    it("Load piece", () => {
        var piece = <Piece>{};
        expect(moves.loadPiece(null)).to.equals(null);
        black.pieces = [];
        white.pieces = [];
        expect(moves.loadPiece({'id': 32, 'color': "white"})).equals(null);
        black.pieces = [piece];
        white.pieces = [piece];
        expect(moves.loadPiece({'id': 0, 'color': "red"})).equals(null);
        black.pieces = [];
        white.pieces = [piece];
        expect(moves.loadPiece({'id': 0, 'color': "white"})).equals(piece);
        black.pieces = [piece];
        white.pieces = [];
        expect(moves.loadPiece({'id': 0, 'color': "black"})).equals(piece);
    });

    it("Encode", () => {

        white.getColor = (): ColorName => {
            return "white";
        }

        black.getColor = (): ColorName => {
            return "black";
        }

        var from = <Square>{
            position: {x: 2, y:3}, 
            getPiece : (): Piece => {
                return <Piece>{id: 32, player: white};
            }
        };
        
        var to = <Square>{
            position: {x: 4, y:5}, 
            getPiece : (): Piece => {
                return <Piece>{id: 11, player: black};
            }
        };

        expect(moves.encode(from, to)).deep.equals({
			'capture' : {'id' : 11, 'color' : "black"},
			'from' : {x: 2, y:3},
			'to' : {x: 4, y:5},
			'piece' : {'id' : 32, 'color' : "white"}
        });   
        
        var from = <Square>{
            position: {x: 2, y:3}, 
            getPiece : () => {
                return null;
            }
        };

        var to = <Square>{
            position: {x: 4, y:5}, 
            getPiece : ()  => {
                return null;
            }
        };

        expect(moves.encode(from, to)).deep.equals({
			'capture' : null,
			'from' : {x: 2, y:3},
			'to' : {x: 4, y:5},
			'piece' : null
        });   

    });

});