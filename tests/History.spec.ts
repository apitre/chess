import History from "Chess/History";
import { expect } from 'chai';
import 'mocha';

describe("Test History", () => {

    var history = new History();

    it("Has next", () => {
         expect(history.hasNext()).equals(false);
    });

});