import Alert from "Chess/Alert/Alert";
import Swal from "sweetalert2";

export default class AlertService
{
    static buildMessage(message: string): Alert
    {
        var alert = new Alert();
        alert.message = message;
        return alert;
    }

    static displayMessage(alert: Alert): void
    {
        Swal.fire(alert.message);
    }

    static dislaySuccessMessage(alert: Alert): void
    {
        Swal.fire({
            position: 'top-end',
            type: 'success',
            title: alert.message,
            showConfirmButton: false,
            timer: 1500
        });
    }
}