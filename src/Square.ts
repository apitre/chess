import Piece from "Chess/Pieces/Piece";
import { Position, PositionRange } from "Chess/Util";
import Player from "Chess/Players/Player";

export default class Square
{
    static readonly size: number = 50;

    public position: Position;
    public classtype: string = "";
	public classes: Array<string> = [];
	public node = undefined;
	public isActive: boolean = false;
	public castling: Position | null = null;
	private piece: Piece | null = null;

    constructor(x: PositionRange, y: PositionRange)
    {
        this.position = {
            x : x, 
            y : y
        };

        if ((x + (y + 1)) % 2 == 0) {
            this.classtype = 'case-dark';
        } else {
            this.classtype = 'case-light';
        }

        this.desactivate();
    };

    public key(): string
    {
        return this.position.x+""+this.position.y;
    }

	public addPiece(piece: Piece){
		this.setPiece(piece, false);
		piece.animate(false);
	};

    public setPiece(piece: Piece, isCaptured: boolean): void
    {   
        if(this.piece != null){
			this.piece.remove();
		}

		this.piece = piece;
		this.piece.square = this;
		this.piece.isCaptured = isCaptured;
	};

    public hasPiece(): boolean
    {
		return this.piece != null;
	};

    public empty(): void
    {
		this.piece = null;
	};

    public desactivate(): void
    {
		this.isActive = false;
		this.classes = [this.classtype];
	};

    public activate(): void
    {
        this.isActive = true;
        this.classes = [this.classtype, 'active'];
	};

    public isEmpty(): boolean
    {
		return this.hasPiece() == false;
	};

    public hasSamePlayer(player: Player)
    {
		return this.piece != null && this.piece.player.getColor() == player.getColor() && player.isOwner;
	};

    public hasEnemyPlayer(player: Player): boolean
    {
		return this.piece != null && this.piece.player.getColor() != player.getColor()
	};

    public hasPieceStarting(): boolean
    {
		return this.piece != null && this.piece.isStarting();
	};

    public hasKing(): boolean
    {
		return this.piece != null && this.piece.is("K");
	};

    public getPiece(): Piece | null
    {
		return this.piece;
	};
}