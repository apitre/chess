import { Position, LinesCollection, PositionRange } from "Chess/Util";
import Square from "Chess/Square";

export default class Board
{
    public lines: LinesCollection = {};

    constructor()
    {
        this.loadLinesCollection();
    }

    private loadLinesCollection(): void
    {
        for(var y = 0; y < 8; y++){
			for(var x = 0; x < 8; x++){
				this.buildSquare(x as PositionRange, y as PositionRange);
			}
		}
    }

    public getSquare(position: Position): Square | null
    {   
		if(this.lines[position.x] != undefined && this.lines[position.x][position.y] != undefined) {
            return this.lines[position.x][position.y];
        }
		return null;
	};

    private buildSquare(x: PositionRange, y: PositionRange): void
    {
        if(this.lines[x] == undefined) {
            this.lines[x] = {};
        }

		this.lines[x][y] = new Square(x, y);
    }
}