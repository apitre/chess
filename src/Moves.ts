import Square from "Chess/Square";
import Board from "Chess/Board";
import Piece from "Chess/Pieces/Piece";
import Player from "Chess/Players/Player";

export default class Moves
{
	private _board: Board;
	private _white: Player;
	private _black: Player;

    constructor(white: Player, black: Player, board: Board)
    {
		this._board = board;
		this._white = white;
		this._black = black;
    }

    public execute(from: Square, to: Square): void
    {
		var piece = from.getPiece();

        if (piece) {
            from.empty();
            to.setPiece(piece, false);
            piece.animate(true);
        }
	}

    public decode(encode: any): any
    {
		return {
			'capture' : this.loadPiece(encode.capture),
			'from' : this._board.getSquare(encode.from),
			'to' : this._board.getSquare(encode.to),
			'piece' : this.loadPiece(encode.piece)
		}
	}

	public encode(from: Square, to: Square): any
	{
		return {
			'capture' : this._encodePiece(to.getPiece()),
			'from' : from.position,
			'to' : to.position,
			'piece' : this._encodePiece(from.getPiece())
		}
	}

    public loadPiece(data: any): Piece | null
    {
		if (data && data.color == 'white' && this._white.pieces[data.id] != undefined) {
			return this._white.pieces[data.id];
		}
		if (data && data.color == 'black' && this._black.pieces[data.id] != undefined) {
			return this._black.pieces[data.id];
		}
		return null;
	};

    private _encodePiece(piece: Piece | null): any
    {
		if (piece) {
			return {'id' : piece.id, 'color' : piece.player.getColor()};
		}
		return null;
	};

	/*

    public prev(): void
    {
		if(this.history.hasPrev()){

			var move = this.history.getMove();

			this.move(move.to, move.from);
			
			if(move.capture != undefined){
				move.to.setPiece(move.capture, true);
			}

			move.piece.removeCount();

			Chess.update(move.piece.player);

			Chess.history.prev();

        }
	}

    public next(): void
    {
		if(Chess.history.hasNext()){

			Chess.history.next();

			var move = Chess.history.getMove();

			private.move(move.from, move.to);

			move.piece.addCount();
			Chess.update(move.piece.player.enemy);

        }
	}

	*/
}