import Piece from "Chess/Pieces/Piece";
import Player from "Chess/Players/Player";
import Board from "Chess/Board";

export default class Queen extends Piece
{
	constructor(player: Player, board: Board)
    {
        super("Q", player, board);
		this.setHtml(['&#9813;', '&#9819;']);
	};

    public setMoves(): void
    {
		this.resetMoves();
		
		this.add(0, -1, false);
		this.add(1, -1, false);
		this.add(1, 0, false);
		this.add(1, 1, false);
		this.add(0, 1, false);
		this.add(-1, 1, false);
		this.add(-1, 0, false);
		this.add(-1, -1, false);
	};

}