import Player from "Chess/Players/Player";
import { PieceType, Direction, Position, PositionRange } from "Chess/Util";
import Square from "Chess/Square";
import Board from "Chess/Board";

export default abstract class Piece
{
	public type: PieceType;
	public moves: Array<Square> = [];
	public node = undefined;
    public player: Player;
    public board: Board;
    public html: string = "";
	public square: Square | null = null;
	public count: number = 0;
	public id: number = 0;
    public isCaptured: boolean = false;
    public direction: Direction = 1;
    
    public abstract setMoves(): void;

    constructor(type: PieceType, player: Player, board: Board)
    {
        this.type = type;
        this.board = board;
		this.player = player;
		this.direction = this.player.isWhite() ? -1 : 1;
		this.id = this.player.pieces.length;
    };
    
    public key(): string
    {
        return this.player.getColor()+this.id;
    }

    public setHtml(html: [string, string]): void
    {
		var index = this.player.isBlack() ? 1 : 0;
		this.html = html[index];
    };

    public getPosition(): Position | null
    {
        if (this.square == null) {
            return null;
        }
		return this.square.position;
	};

    public is(type: PieceType): boolean
    {
		return this.type == type;
	};

    public animate(doAnimation: boolean): void
    {
		if (doAnimation) {
            //$(this.node).animate(this.square.getOffset());
        } else {
            //$(this.node).css(this.square.getOffset());
        }
    };
    
    public removeCount(): void
    {
		this.count--;
	};

    public addCount(): void
    {
		this.count++;
	};

    public goTo(to: any): void
    {
		//Chess.moves.goTo(this.square, to);
	};

    public isStarting(): boolean
    {
		return this.count == 0;
	};

    public showMoves(): void
    {
		for(var i = 0; i < this.moves.length; i++) {
            this.moves[i].activate();
        }
	};

    public hideMoves(): void
    {
		for(var i = 0; i < this.moves.length; i++) {
            this.moves[i].desactivate();
        }
	};

    public isNotValidMove(square: Square): boolean
    {
		return square.hasSamePlayer(this.player);
	};

    public resetMoves(): void
    {
		this.moves = [];
	};

    public remove(): void
    {
		this.isCaptured = true;
		this.square = null;
	};

    public getStartingRow(): PositionRange
    {
		if(this.is("p")){
            return this.player.isWhite() ? 6 : 1;
        }
		return this.player.isWhite() ? 7 : 0;
	};

    public addMoves(square: Square): boolean
    {   
		if (this.isNotValidMove(square)) {
			return false;
		}

		if (square.hasEnemyPlayer(this.player)) {
			this.moves.push(square);
			return false;
		}

		if (square.isEmpty()) {
			this.moves.push(square);
			return true;
        }
        
        return false;
	};

    public add(x: number, y: number, isSingle: boolean): void
    {
		var position = this.getPosition();
        var next = true;    
        
        if (position) {

            var nextPosition = {
                x: position.x,
                y: position.y
            };

            while (next == true) {

                nextPosition.x += x;
                nextPosition.y += y * this.direction;
    
                var square = this.board.getSquare(nextPosition);
                
                next = square ? this.addMoves(square) : false;
    
                if (isSingle) {
                    next = false;
                }
    
            }
        }

	};
}