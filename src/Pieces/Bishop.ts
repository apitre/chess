import Piece from "Chess/Pieces/Piece";
import Player from "Chess/Players/Player";
import Board from "Chess/Board";

export default class Bishop extends Piece
{
    constructor(player: Player, board: Board)
    {
        super("B", player, board);
		this.setHtml(['&#9815;', '&#9821;']);
	};

    public setMoves(): void
    {
		this.resetMoves();

		this.add(1, -1, false);
		this.add(1, 1, false);
		this.add(-1, 1, false);
		this.add(-1, -1, false);
	};
}