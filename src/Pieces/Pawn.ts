import Piece from "Chess/Pieces/Piece";
import Player from "Chess/Players/Player";
import Square from "Chess/Square";
import Board from "Chess/Board";
import { PositionRange } from "Chess/Util";

export default class Pawn extends Piece
{
	constructor(player: Player, board: Board)
    {
        super("p", player, board);
		this.setHtml(['&#9817;', '&#9823;']);
	};

    public addMoves(square: Square): boolean
    {
		if (this.isNotValidMove(square)) {
			return false
        }
        
        var position = this.getPosition();

		if (square.hasEnemyPlayer(this.player)) {
			if (position == null || position.x != square.position.x) {
                this.moves.push(square);
            }
            return false;
		}

		if (square.isEmpty() && position != null && position.x == square.position.x) {
			this.moves.push(square);
			return true;
		}

        return true;
	};

    public setMoves(): void
    {
		this.resetMoves();
        
		this.add(0, 1, true);
		this.add(1, 1, true);
        this.add(-1, 1, true);
        
		if (this.isStarting() && this.isEmptyFront()) {
            this.add(0, 2, true);
        }
	};

    private isEmptyFront(): boolean
    {
        var position = this.getPosition();
        
        if (position == null) {
            return false;
        }
        
        var square = this.board.getSquare({
            x: position.x,
            y: (position.y + this.direction) as PositionRange
        });

		return square ? square.isEmpty() : false;
	}
}