import Piece from "Chess/Pieces/Piece";
import Player from "Chess/Players/Player";
import Board from "Chess/Board";

export default class Knight extends Piece
{
	constructor(player: Player, board: Board)
    {
        super("K", player, board);
		this.setHtml(['&#9816;', '&#9822;']);
	};

    public setMoves(): void
    {
		this.resetMoves();

		this.add(-2, -1, true);
		this.add(-1, -2, true);
		this.add(2, -1, true);
		this.add(1, -2, true);
		this.add(-2, 1, true);
		this.add(-1, 2, true);
		this.add(2, 1, true);
		this.add(1, 2, true);
	};
}