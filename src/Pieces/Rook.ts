import Piece from "Chess/Pieces/Piece";
import Player from "Chess/Players/Player";
import { Position } from "Chess/Util";
import Board from "Chess/Board";

export default class Rook extends Piece
{
	public castling: Position | null = null;

	constructor(player: Player, board: Board)
    {
        super("R", player, board);
		this.setHtml(['&#9814;', '&#9820;']);
	};

	public setCastling(position: Position){
		position.x = (position.x == 0) ? 3 : 5;
		this.castling = position;
	};

    public setMoves(): void
    {
		this.resetMoves();
		
		this.add(0, -1, false);
		this.add(0, 1, false);
		this.add(1, 0, false);
		this.add(-1, 0, false);
	};
}