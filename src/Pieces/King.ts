import Piece from "Chess/Pieces/Piece";
import Player from "Chess/Players/Player";
import { Position, PositionRange } from "Chess/Util";
import Board from "Chess/Board";

export default class King extends Piece
{
	constructor(player: Player, board: Board)
    {
        super("K", player, board);
		this.setHtml(['&#9812;', '&#9818;']);
	};

    public setMoves(): void
    {
		this.resetMoves();

		this.add(0, -1, true);
		this.add(1, -1, true);
		this.add(1, 0, true);
		this.add(1, 1, true);
		this.add(0, 1, true);
		this.add(-1, 1, true);
		this.add(-1, 0, true);
		this.add(-1, -1, true);

		if(this.hasRightCastling()) {
            this.addCastling({'x' : 6, 'y' : this.getStartingRow()});
        }

		if(this.hasLeftCastling())
			this.addCastling({'x' : 2, 'y' : this.getStartingRow()});

	};

    public addCastling(destination: Position): void
    {
        var square = this.board.getSquare(destination);
        
        if (square != null) {
            var x: PositionRange = destination.x == 2 ? 0 : 7;
            square.castling = {'x' : x, 'y' : destination.y};
            this.moves.push(square);
        }
	};

    public hasTower(position: Position): boolean
    {
        var square = this.board.getSquare(position);
        
        if (square == null) {
            return false;
        }

        var tower = square.getPiece();
        
		return tower != null && tower.isStarting() && this.isStarting();
    };
    
    private isEmptySquare(position: Position): boolean
    {
        var square = this.board.getSquare(position);
        return square ? square.isEmpty() : false;
    }

    public hasRightCastling(): boolean
    {
		var y = this.getStartingRow();
		var isEmptyA = this.isEmptySquare({'x' : 5, 'y' : y});
		var isEmptyB = this.isEmptySquare({'x' : 6, 'y' : y});
		return isEmptyA && isEmptyB && this.hasTower({'x' : 1, 'y' : y});
	};

    public hasLeftCastling(): boolean
    {
		var y = this.getStartingRow();
		var isEmptyA = this.isEmptySquare({'x' : 1, 'y' : y});
		var isEmptyB = this.isEmptySquare({'x' : 2, 'y' : y});
		var isEmptyC = this.isEmptySquare({'x' : 3, 'y' : y});
		return isEmptyA && isEmptyB && isEmptyC && this.hasTower({'x' : 7, 'y' : y});
	};
}