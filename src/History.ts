export default class History
{
    public list: Array<any> = [];

	private pointer: number = -1;
	private x = ['a','b','c','d','e','f','g','h'];
	private y = ['1','2','3','4','5','6','7','8'];

    private isLastMove(): boolean
    {
		return this.hasNext() == false;
	};

    public save(move: any): void
    {
		this.pointer++;

		if (this.list[this.pointer] != undefined) {
            this.slice(this.pointer);
        }

        this.list.push(move);
        
		this.updateLastMove();
	};

    private slice(index: number): void
    {
        this.list = this.list.slice(0, index);

        /*
		this.list = this.list.slice(0, index);

		var nbMoves = this.panel.childNodes.length - index;

		for(var i = 0; i < nbMoves; i++){
			this.panel.removeChild(this.panel.childNodes[index]);
        }
        */
	};

    public encodeNotation(move: any): string
    {
		var capture = "";
		var position = move.to.position;
		var type = move.piece.type.toUpperCase();

		if (move.piece.type == "p" && move.piece.count == 0) {
            type = "";
        }

		if (move.capture != undefined) {
            capture = "x";
        }

		return type + capture + this.x[position.x] + this.y[position.y];
	};

    private updateLastMove(): void
    {
        /*
		var lastMove = this.panel.querySelector(".last-move");

		if(lastMove != undefined)
			lastMove.removeAttribute('class');

		var pointerMove = this.panel.childNodes[this.pointer];

		if (pointerMove != undefined) {
            pointerMove.className = "last-move";
        }
        */
	};

    public hasNext(): boolean
    {
		return this.list[this.pointer + 1] != undefined;
	};

    public hasPrev(): boolean
    {
		return this.pointer > -1;
	};

    public getMove(): any
    {
		return this.list[this.pointer];
	};

    public next(): void
    {
		this.pointer++;
		this.updateLastMove();
	};

    public prev(): void
    {
		this.pointer--;
		this.updateLastMove();
	};
}