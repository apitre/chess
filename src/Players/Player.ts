import { ColorName, Position, Pieces } from "Chess/Util";
import Piece from "Chess/Pieces/Piece";
import Square from "Chess/Square";

export default class Player
{
    private color: ColorName = 'white';
    public pieces: Array<Piece> = [];
    public isOwner: boolean = false;
    public isCheck: boolean = false;
    public isPlaying: boolean = false;
	public enemy: Player | null = null;
    
    constructor(color: ColorName, isOwner: boolean)
    {
        this.color = color;
        this.isOwner = isOwner;
    }

    public isWhite(): boolean
    {
		return this.color == "white";
	}

    public isBlack(): boolean
    {
		return this.color == "black";
    }
    
    public addPiece(piece: Piece): void
    {
        this.pieces.push(piece);
    }
    
    public getColor(): ColorName
    {
        return this.color;
    }

    public updateMoves(): void
    {
        for (var i = 0; i < this.pieces.length; i++) {
            if(this.pieces[i].isCaptured == false){
				this.pieces[i].setMoves();
				this.analyse(this.pieces[i].moves);
			}
        }
    };
    
    public analyse(moves: Array<Square>): void
    {
        for (var i = 0; i < moves.length; i++) {
            if(moves[i].hasKing() && this.enemy){
                this.enemy.isCheck = true;
                break;  
			}
        }
	};
}