import { ColorName, Position, PositionRange } from "Chess/Util";
import Player from "Chess/Players/Player";
import Bishop from "Chess/Pieces/Bishop";
import Pawn from "Chess/Pieces/Pawn";
import Knight from "Chess/Pieces/Knight";
import Rook from "Chess/Pieces/Rook";
import Queen from "Chess/Pieces/Queen";
import King from "Chess/Pieces/King";
import Board from "Chess/Board";
import Piece from "Chess/Pieces/Piece";

export default class PlayersFactory
{
    private board: Board;

    constructor(board: Board)
    {
        this.board = board;
    }

    public create(color: ColorName, isOwner: boolean): Player
    {
        var player = new Player(color, isOwner);

        this._bindPieceToGame(player, new Pawn(player, this.board), 0);
		this._bindPieceToGame(player, new Pawn(player, this.board), 1);
		this._bindPieceToGame(player, new Pawn(player, this.board), 2);
		this._bindPieceToGame(player, new Pawn(player, this.board), 3);
		this._bindPieceToGame(player, new Pawn(player, this.board), 4);
		this._bindPieceToGame(player, new Pawn(player, this.board), 5);
		this._bindPieceToGame(player, new Pawn(player, this.board), 6);
        this._bindPieceToGame(player, new Pawn(player, this.board), 7);
        
        this._bindPieceToGame(player, new Rook(player, this.board), 0);
        this._bindPieceToGame(player, new Rook(player, this.board), 7);
        
        this._bindPieceToGame(player, new Knight(player, this.board), 1);
        this._bindPieceToGame(player, new Knight(player, this.board), 6);
        
        this._bindPieceToGame(player, new Bishop(player, this.board), 2);
        this._bindPieceToGame(player, new Bishop(player, this.board), 5);
        
        this._bindPieceToGame(player, new Queen(player, this.board), 3);
        this._bindPieceToGame(player, new King(player, this.board), 4);

		return player;
    }

    private _bindPieceToGame(player: Player, piece: Piece, x: PositionRange): void
    {        
        player.addPiece(piece);

		var square = this.board.getSquare({
            'x' : x, 
            'y' : piece.getStartingRow()
        });

        if (square) {
            square.addPiece(piece);
        }
	};


}