import Piece from "Chess/Pieces/Piece";
import Square from "Chess/Square";

export type ColorName = "white" | "black";

export type ColorID = 1 | 2;

export type PositionRange = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7;

export type Position = {
	x: PositionRange 
	y: PositionRange
};

export type SquaresCollection = {
	[key:number]: Square
}

export interface StylesObject 
{
    [key:string]: string | number;
}

export type LinesCollection = {
	[key:number]: SquaresCollection
}

export type Pieces = { [key:string]: Piece; }

export type PieceType = "B" | "K" | "N" | "p" | "R" | "Q";

export type Direction = 1 | -1;