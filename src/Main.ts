import Chess from "Chess/Components/Chess.vue";
import { ColorID } from "Chess/Util";
import PlayersFactory from "./Players/PlayersFactory";
import Board from "Chess/Board";
import Moves from "Chess/Moves";
import History from "Chess/History";

export default class Game
{   
    private vue: any = null;
    private window: Window;
    private document: Document;

    constructor(window: Window, document: Document)
    {
        this.window = window;
        this.document = document;
    }

    public run(selector: string, colorId: ColorID): void
    {
        var board = new Board();

        var history = new History();

        var playersFactory = new PlayersFactory(board)

        var white = playersFactory.create('white', colorId == 1);
        var black = playersFactory.create('black', colorId == 2);

        white.enemy = black;
        black.enemy = white;

        var moves = new Moves(white, black, board);
        
        this.vue = new Chess({
            el: selector,
            data: {
                window: window,
                document: document,
                board: board,
                black: black,
                white: white,
                moves: moves,
                history: history
            }
        });
    }
}

var chess = new Game(window, document);

chess.run('#chess', 1);