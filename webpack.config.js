const path = require('path');
const fs = require('fs');
const { VueLoaderPlugin } = require('vue-loader');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const TerserPlugin = require('terser-webpack-plugin');

var bundle = (argv) =>
{       
    var distribution = '/dist/';

    if (argv.dist) {
        distribution = argv.dist
        console.log("------------------------------------------------");
        console.log("BUILD DISTRIBUTION AT: "+distribution);
        console.log("------------------------------------------------\n");
    }

    var config = {
        entry: {
            chess: './src/Main.ts',
        },
        output: {
            path: distribution,
            publicPath: distribution,
            filename: '[name].js',
            chunkFilename: '[name].bundle.js'
        },
        module: {
            rules: [
                {
                    test: /\.css$/,
                    use: ['style-loader', 'css-loader']
                },
                {
                    test: /\.vue$/,
                    loader: 'vue-loader'
                },
                {
                    test: /\.(ts|tsx)?$/,
                    loader: 'ts-loader',
                    exclude: /node_modules/,
                    options: {
                        appendTsSuffixTo: [/\.vue$/],
                    }
                },
                {
                    test: /\.(png|jpg|gif|svg|eot|svg|woff2|ttf|woff)$/,
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]?[hash]'
                    }
                }
            ]
        },
        resolve: {
            extensions: ['.ts', '.js', '.vue', '.json', '.css'],
            alias: {
                'vue$': 'vue/dist/vue.esm.js',
                'Chess': path.join(__dirname, "src")
            }
        },
        plugins: [
            new VueLoaderPlugin()
        ]
    };

    if (argv.analyze && argv.analyze == 'yes') {
        config.plugins.push(new BundleAnalyzerPlugin());
    }

    if (argv.dist) {
        config.optimization = {
            minimizer: [
                new TerserPlugin({
                    terserOptions: {
                        output: {
                            comments: false,
                        }
                    },
                })
            ]
        }
    }

    if (argv.mode == 'development') {
        config.devServer = {
            contentBase: './dist',
            disableHostCheck: true,
            port: 9000
        };
        config.devtool = 'inline-source-map';
    }
    
    return config;
}

module.exports = (env, argv) => bundle(argv);